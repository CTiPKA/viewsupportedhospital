//
//  HospitalDetailsViewController.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import UIKit

class HospitalDetailsViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phones: UILabel!
    @IBOutlet weak var specialities: UILabel!
    
    var hospitalsTools = HospitalsTools()
    var hospital = Hospital()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Hospital Details"
        self.nameLabel.text! = hospital.name
        self.phones.text! = ", ".join(hospital.phones)
        self.specialities.text! = ", ".join(hospital.specialities)
        
        logo.contentMode = UIViewContentMode.ScaleAspectFit
        if let imageUrl = NSURL(string: hospital.photo) {
            
            //put placeholder during downloading
            self.logo.image = UIImage(named: "img_download")
            
            hospitalsTools.downloadImage(imageUrl, completion: { (image) -> Void in
                if (image != nil) {
                    self.logo.image = image
                } else {
                    self.logo.image = UIImage(named: "img_download_err")
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableView methods
    func numberOfSections() -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return hospital.doctors.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell? {
        //variable type is inferred
        var cell = tableView.dequeueReusableCellWithIdentifier("Doctor Info") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "Doctor Info")
        }
        
        let doctor = hospital.doctors[indexPath.row]
        //we know that cell is not empty now so we use ! to force unwrapping
        cell!.textLabel!.text = doctor.firstName + " " + doctor.lastName
        if !doctor.specialities.isEmpty {
            //println("specialities - " + ", ".join(doctor.specialities))
            cell!.detailTextLabel!.text = ", ".join(doctor.specialities)
        }
        
        if ((indexPath.row % 2) == 0) {
            cell!.backgroundColor = UIColor (red: 243.0/255.0, green: 241.0/255.0, blue: 235.0/255.0, alpha: 1)
        } else {
            cell!.backgroundColor = UIColor (red: 235.0/255.0, green: 231.0/255.0, blue: 218.0/255.0, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }

}