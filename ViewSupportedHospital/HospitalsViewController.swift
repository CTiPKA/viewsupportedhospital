//
//  ViewController.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import UIKit

class HospitalsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var hospitalsData = HospitalsAppData()
    var selectedHospital = Hospital()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Hospitals List"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableView methods
    func numberOfSections() -> Int {
        return 1
    }

    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return hospitalsData.hospitals.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell? {
        //variable type is inferred
        var cell = tableView.dequeueReusableCellWithIdentifier("Hospital Info") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "Hospital Info")
        }
        
        let hospital = hospitalsData.hospitals[indexPath.row]
        //we know that cell is not empty now so we use ! to force unwrapping
        cell!.textLabel!.text = hospital.name
        
        if ((indexPath.row % 2) == 0) {
            cell!.backgroundColor = UIColor (red: 243.0/255.0, green: 241.0/255.0, blue: 235.0/255.0, alpha: 1)
        } else {
            cell!.backgroundColor = UIColor (red: 235.0/255.0, green: 231.0/255.0, blue: 218.0/255.0, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let hospital = hospitalsData.hospitals[indexPath.row]
        selectedHospital = hospital
        performSegueWithIdentifier("Hospital Details", sender: self)
    }
    
    // MARK: flow
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let hospitalDetailsController = segue.destinationViewController as! HospitalDetailsViewController
        hospitalDetailsController.hospital = selectedHospital
    }
    
}