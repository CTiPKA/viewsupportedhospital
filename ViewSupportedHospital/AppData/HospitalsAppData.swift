//
//  HospitalsAppData.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import Foundation

class HospitalsAppData
{
    var hospitals = [Hospital]()
    private var doctors = [Doctor]()
    
    private func composeDoctorsLocal () {
        var doctor1 = Doctor(
            firstName: "John",
            lastName: "Smith",
            specialities: ["Spec 1", "Spec 2"]
        )
        doctors.append(doctor1)
        
        var doctor2 = Doctor(
            firstName: "Pall",
            lastName: "Smith",
            specialities: ["Spec 3", "Spec 4"]
        )
        doctors.append(doctor2)
        
        var doctor3 = Doctor(
            firstName: "Napoleon",
            lastName: "Smith",
            specialities: ["Spec 5", "Spec 6"]
        )
        doctors.append(doctor3)
        
        var doctor4 = Doctor(
            firstName: "Amely",
            lastName: "Smith",
            specialities: ["Spec 7", "Spec 8"]
        )
        doctors.append(doctor4)
    }
    
    private func composeHospitalsLocal () {
        var hospital1 = Hospital(
            name: "First Royal Hospital",
            phones: ["+8001002001", "+8001002002", "+8001002003", "+8001002004"],
            doctors: [doctors[0], doctors[1], doctors[2]],
            photo: "http://news.eastvillagers.org/wp-content/uploads/2011/05/hospital_cartoon.jpg",
            specialities: ["Spec 1", "Spec 2"]
        )
        hospitals.append(hospital1)
        
        var hospital2 = Hospital(
            name: "Second Hospital",
            phones: ["+8001002001", "+8001002004"],
            doctors: [doctors[2]],
            photo: "",
            specialities: ["Spec 1", "Spec 2"]
        )
        hospitals.append(hospital2)
        
        var hospital3 = Hospital(
            name: "Just Hospital",
            phones: ["+8001002003", "+8001002004"],
            doctors: [doctors[1]],
            photo: "http://news.eastvillagers.org/wp-content/uploads/2011/05/hospital_cartoon.jpg",
            specialities: ["Spec 1", "Spec 2"]
        )
        hospitals.append(hospital3)
        
        var hospital4 = Hospital(
            name: "Hospital 4",
            phones: ["+8001002001", "+8001002002", "+8001002003"],
            doctors: [doctors[2], doctors[3]],
            photo: "",
            specialities: ["Spec 1", "Spec 2"]
        )
        hospitals.append(hospital4)
        
        var hospital5 = Hospital(
            name: "Last Hospital",
            phones: ["+8001002001", "+8001002002", "+8001002003", "+8001002004", "+8001002003", "+8001002004"],
            doctors: [doctors[0], doctors[1], doctors[2], doctors[3]],
            photo: "http://news.eastvillagers.org/wp-content/uploads/2011/05/hospital_cartoon.jpg",
            specialities: ["Spec 1", "Spec 2"]
        )
        hospitals.append(hospital5)
        hospitals.append(hospital1)
        hospitals.append(hospital2)
        hospitals.append(hospital3)
        hospitals.append(hospital4)
        hospitals.append(hospital5)
    }
    
    init () {
        //compose hospitals local data
        composeDoctorsLocal()
        composeHospitalsLocal()
    }

}