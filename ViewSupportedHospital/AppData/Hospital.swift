//
//  Hospital.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import Foundation

class Hospital
{
    var name = String()
    var phones = [String]()
    var doctors = [Doctor]()
    var photo = String()
    var specialities = [String]()
    
    init () {
        
    }
    
    init (name:String, phones:[String], doctors:[Doctor], photo:String, specialities:[String]) {
        self.name = name
        self.phones = phones
        self.doctors = doctors
        self.photo = photo
        self.specialities = specialities
    }
}