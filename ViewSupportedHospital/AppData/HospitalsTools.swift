//
//  HospitalsTools.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import Foundation
import UIKit

class HospitalsTools
{
    init () {
        
    }
    
    func getDataFromUrl(url: NSURL, completion: ((data: NSData?) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data)
            }.resume()
    }
    
    func downloadImage(url: NSURL, completion:((image: UIImage?) -> Void )){
        getDataFromUrl(url) { data in
            dispatch_async(dispatch_get_main_queue()) {
                completion (image:UIImage(data: data!))
            }
        }
    }
}