//
//  Doctor.swift
//  ViewSupportedHospital
//
//  Created by Vadim Trulyaev on 7/23/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import Foundation

class Doctor
{
    var firstName = String()
    var lastName = String()
    var specialities = [String]()
    
    init () {
        
    }
    
    init (firstName:String, lastName:String, specialities:[String]) {
        self.firstName = firstName
        self.lastName = lastName
        self.specialities = specialities
    }
}